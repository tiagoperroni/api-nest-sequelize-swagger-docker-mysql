import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './user.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User)
    private readonly userModel: typeof User,    
  ) {}
  

  async findAll(): Promise<User[]> {
    const user = await this.userModel.findAll();
    return user;
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userModel.findOne({
      where: { id, }, 
    });
    return user;
  }

  async create(user: User): Promise<User> {
    return this.userModel.create(user);
  }

  async update(id: number, newUser: User) {   
    await this.userModel.update(newUser, {
        where: { id, },
    });
    const user = this.findOne(id);
    return user;
    
  }

  async delete(id: number): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }
}